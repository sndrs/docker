## Docker DEV environment
### Structure 
├── docker-compose.yml  
├── mysql  
│   ├── db-data  
│   └── init.sql  
├── nginx  
│   └── conf  
├── php  
│   ├── conf  
│   └── Dockerfile  
└── src ( [project's code, see sndrs/web repository](https://bitbucket.org/sndrs/web))  

### Setup
1. run `git clone --recursive https://bitbucket.org/sndrs/docker`
2. configure that your DNS/hosts file points `web.test` to the docker instance
3. add your TLS certificates (see `docker-compose.yml`)
2. run `docker-compose up -d`

### Code
[sndrs/web repository](https://bitbucket.org/sndrs/web)
