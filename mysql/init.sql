--
-- File gets run when container gets created

-- Creates table for unit tests run by PHPUnit
CREATE DATABASE IF NOT EXISTS `web.test-testing`;

GRANT ALL ON `web.test-testing`.* to `web.test`;

-- Creates table for functional tests run by Laravel Dusk
CREATE DATABASE IF NOT EXISTS `web.test-dusk`;

GRANT ALL ON `web.test-dusk`.* to `web.test`;
